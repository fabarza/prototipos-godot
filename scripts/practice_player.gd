extends CharacterBody3D


const SPEED = 5.0
const CLIMB_SPEED = 3.0
const JUMP_VELOCITY = 7.0
const MOUSE_SENSE = 0.001

@onready var spring_arm_pivot = $SpringArmPivot
@onready var spring_arm = $SpringArmPivot/SpringArm3D

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
# Detecta si se encuentra escalando una pared o no
var is_climbing = false

func _ready():
	# Configura el modo del ratón para que esté capturado (oculto y confinado a la ventana)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _unhandled_input(event: InputEvent) -> void:
	if Input.is_action_just_pressed("ui_cancel"):
		# Cierra la ventana, finaliza el prototipo
		get_tree().quit()
	if event is InputEventMouseMotion:
		spring_arm_pivot.rotate_y(-event.relative.x * MOUSE_SENSE)
		spring_arm.rotate_x(-event.relative.y * MOUSE_SENSE)
		spring_arm.rotation.x = clamp(spring_arm.rotation.x, -1, 1)

func _physics_process(delta):
	if is_climbing:
		_climb_handle(delta)
	else:
		_move_handle(delta)
	
	# Cuando se esta tocando una pared, el player empezara a escalar si presiona la tecla "W"
	if Input.is_action_pressed("move_forward") and is_on_wall() and not is_climbing:
		is_climbing = true
		print("Climbing...")
	
	# Cuando se esta tocando la pared y el piso al mismo tiempo, el player dejara de escalar
	# pared si se presiona la tecla "S".
	if Input.is_action_pressed("move_back") and is_on_floor() and is_on_wall():
		is_climbing = false
		print("Not climbing...")
	
	# Cuando el player esta escalando, este puede dejar de escalar presionando el "Espacio"
	if Input.is_action_just_pressed("move_jump") and is_climbing:
		is_climbing = false
		print("Not climbing...")
	
	# Cuando el player llegue a algun borde, este dejara de escalar y se desprendera de la pared
	if is_climbing and not is_on_wall() and (Input.is_action_pressed("move_forward") or Input.is_action_pressed("move_back") or Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right")):
		is_climbing = false
		print("Not climbing...")

func _move_handle(delta: float) -> void:
	# Gravedad cuando se encuentra en el aire
	if not is_on_floor():
		velocity.y -= gravity * delta * 2
	
	# Realiza un salto cuando se preciona el "Espacio"
	if Input.is_action_just_pressed("move_jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
	
	# Cuando se encuentra caminando en el piso
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_back")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	direction = direction.rotated(Vector3.UP, spring_arm_pivot.rotation.y)
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
	
	move_and_slide()
	
# Funcion que maneja el movimiento mientras se escala
func _climb_handle(delta: float) -> void:
	var input_dir = Input.get_vector("move_left", "move_right", "move_back", "move_forward")
	# Determina en que eje estoy escalando, permitiendome hacia los lados en cualquier pared mientras escala
	var wall_direction = ((-input_dir.x) * get_wall_normal().cross(Vector3.UP).normalized()) + Vector3(0, input_dir.y, 0)
	var direction = (transform.basis * wall_direction).normalized()
	if direction:
		velocity.x = direction.x * CLIMB_SPEED
		velocity.y = direction.y * CLIMB_SPEED
		velocity.z = direction.z * CLIMB_SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, CLIMB_SPEED)
		velocity.y = move_toward(velocity.y, 0, CLIMB_SPEED)
		velocity.z = move_toward(velocity.z, 0, CLIMB_SPEED)
	
	move_and_slide()
