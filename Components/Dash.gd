extends Node

class_name Dash

# Variables de clase para configuración y estado
var initial_speed: float = 0.0
var dash_duration: float = 0.0
var cooldown_duration: float = 0.0
var dash_status: bool = false
var cooldown_status: bool = false
var dash_accumulated_timer: float = 0.0
var cooldown_accumulated_timer: float = 0.0


func start(speed: float, duration: float, cooldown: float) -> void:
	initial_speed = speed
	dash_duration = duration
	cooldown_duration = cooldown
	dash_status = true
	cooldown_status = false
	dash_accumulated_timer = 0.0
	cooldown_accumulated_timer = 0.0
	
func stop() -> void:
	dash_accumulated_timer = 0.0
	cooldown_accumulated_timer = 0.0
	dash_status = false
	cooldown_status = false
	
func update_timer(delta: float) -> void:
	if dash_status:
		dash_accumulated_timer += delta
		if dash_accumulated_timer > dash_duration:
			dash_status = false
			if cooldown_duration > 0:
				cooldown_status = true
				cooldown_accumulated_timer = 0.0
			else:
				stop()
	
	if is_in_cooldown:
		cooldown_accumulated_timer += delta
		if cooldown_accumulated_timer > cooldown_duration:
			stop()

func get_speed() -> float:
	# Constante de decrecimiento
	var k: float = 2.0 / dash_duration
	
	# Calcular la velocidad decreciente
	var val: float = initial_speed * exp(-k * dash_accumulated_timer)
	
	# Retornar la velocidad calculada
	return val

func is_active() -> bool:
	return dash_status

func is_in_cooldown() -> bool:
	return cooldown_status
