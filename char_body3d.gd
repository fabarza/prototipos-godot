extends CharacterBody3D

class_name Player

@export var SPEED = 5.0
@export var DASH_SPEED = 20.0  # Velocidad del dash
@export var DASH_DURATION = 1
@export var DASH_COOLDOWN = 0
@export var JUMP_VELOCITY = 4.5

const DASH_COLOR = Color(1, 0, 0)  # Rojo
const NORMAL_COLOR = Color(1, 1, 1)  # Blanco (o el color normal del personaje)
const COOLDOWN_COLOR = Color(1, 1, 0)  # Amarillo

var dash_handler: Dash
var dash_direction: Vector3 = Vector3.ZERO  # Dirección del dash

# Sensibilidad del ratón para rotación
var mouse_sensitivity := 0.001
# Entrada de rotación en el eje Y (torsión)
var twist_input := 0.0
# Entrada de rotación en el eje X (inclinación)
var pitch_input := 0.0

# Obtiene la gravedad desde la configuración del proyecto para sincronizar con los nodos RigidBody.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

# Referencia al nodo TwistPivot
@onready var twist_pivot := $TwistPivot
# Referencia al nodo PitchPivot
@onready var pitch_pivot := $TwistPivot/PitchPivot
# Referencia al mesh del personaje
@onready var mesh_instance := $MeshInstance3D

# Llamado cuando el nodo entra en el árbol de la escena por primera vez.
func _ready() -> void:
	# Configura el modo del ratón para que esté capturado (oculto y confinado a la ventana)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	dash_handler = Dash.new()

# Llamado en cada frame para manejar la física.
func _physics_process(delta):
	# Añade la gravedad.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Maneja el salto.
	if Input.is_action_just_pressed("move_jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Obtiene la dirección de entrada y maneja el movimiento/desaceleración.
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_back")
	# Se aplica en la dirección en la que se está mirando, la posición del twist pivot.
	var direction = (twist_pivot.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()

	# Maneja el dash.
	if Input.is_action_just_pressed("action_dash") and not dash_handler.is_active() and not dash_handler.is_in_cooldown():
		dash_handler.start(DASH_SPEED, DASH_DURATION, DASH_COOLDOWN)
		dash_direction = direction  # Almacena la dirección del dash
		change_color(DASH_COLOR)  # Cambia el color al hacer dash

	# Actualiza el timer del dash.wd
	dash_handler.update_timer(delta)
	
	if dash_handler.is_active():	
		velocity.x = dash_direction.x * (SPEED + dash_handler.get_speed())
		velocity.z = dash_direction.z * (SPEED + dash_handler.get_speed())
	else:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	
	if not dash_handler.is_active() and not dash_handler.is_in_cooldown():
		change_color(NORMAL_COLOR)  # Restaura el color normal después del dash
	
	# Verifica el estado del cooldown y actualiza el color solo cuando cambie el estado
	if dash_handler.is_in_cooldown() and not dash_handler.is_active():
		change_color(COOLDOWN_COLOR)  # Cambia al color de cooldown
	
	# Aplica el movimiento.
	move_and_slide()
	handle_camera_movement()
	handle_collisions()
	
# Maneja las colisiones y realiza el rebote si el dash está activo.
func handle_collisions():
	var direction_changed = false
	for i in range(get_slide_collision_count()):
		var collision = get_slide_collision(i)
		if collision and dash_handler.is_active() and not direction_changed:
			dash_direction = dash_direction.bounce(collision.get_normal(i))  # Cambia la dirección del dash al vector de rebote
			direction_changed = true


# Llamado cuando hay entrada que no ha sido manejada (como movimiento del ratón).
func _unhandled_input(event: InputEvent) -> void:
	# Si el evento es movimiento del ratón.
	if event is InputEventMouseMotion:
		# Si el ratón está capturado (modo de entrada activo).
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			# Ajusta la entrada de torsión e inclinación basándose en el movimiento relativo del ratón.
			twist_input = -event.relative.x * mouse_sensitivity
			pitch_input = -event.relative.y * mouse_sensitivity
	# Si se presiona la tecla R, la escena actual se reiniciará.
	if event.is_action_pressed("restart_level"):
		# Esto reinicia la escena actual.
		get_tree().reload_current_scene()

# Maneja el movimiento de la cámara basado en la entrada del ratón.
func handle_camera_movement() -> void:
	# Si se presiona la acción de cancelar (usualmente la tecla 'Esc').
	if Input.is_action_just_pressed("ui_cancel"):
		# Cambia el modo del ratón a visible.
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
	# Rota el nodo TwistPivot en el eje Y (torsión).
	twist_pivot.rotate_y(twist_input)
	# Rota el nodo PitchPivot en el eje X (inclinación).
	pitch_pivot.rotate_x(pitch_input)
	# Limita la rotación en el eje X de PitchPivot para evitar giros excesivos.
	pitch_pivot.rotation.x = clamp(
		pitch_pivot.rotation.x,
		-1,
		1
	)
	# Reinicia las entradas de rotación para el siguiente fotograma.
	twist_input = 0.0
	pitch_input = 0.0

# Cambia el color del personaje
func change_color(color: Color) -> void:
	var surf_material = mesh_instance.get_surface_override_material(0)
	if surf_material:
		surf_material.albedo_color = color
